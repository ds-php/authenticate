<?php

namespace Tests\Authenticate;

use Ds\Authenticate\Password;
use Ds\Authenticate\PasswordInterface;

/**
 * Class PasswordTest
 *
 * @package Tests\Authenticate
 */
class PasswordTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var PasswordInterface
     */
    public $password;

    /**
     *
     */
    protected function setUp() : void
    {
        $this->password = new Password();
    }

    /**
     *
     */
    public function testNeedsRehash()
    {
        $password = 'password';
        $hash = \password_hash('password', PASSWORD_DEFAULT);
        $result = $this->password->needsRehash($password, $hash, PASSWORD_DEFAULT, 12);
        $this->assertNotEquals($hash, $result);
    }

    /**
     *
     */
    public function testNeedsRehashNoRehash()
    {
        $password = 'password';
        $hash = password_hash('password', PASSWORD_DEFAULT, ['cost' => 12]);
        $result = $this->password->needsRehash($password, $hash, PASSWORD_DEFAULT, 12);
        $this->assertEquals(false, $result);
    }

    /**
     * @param string $password
     * @dataProvider hashPasswordProvider
     */
    public function testHash($password)
    {
        $hashedPassword = $this->password->hash($password);
        $this->assertEquals(true, $this->password->verify($password, $hashedPassword));
    }

    /**
     * @param string $password
     * @param string $hashedPassword
     * @dataProvider truePasswordProvider
     */
    public function testVerify($password, $hashedPassword)
    {
        $passwordHash = $this->password->verify($password, $hashedPassword);
        $this->assertEquals($passwordHash, true);
    }

   
     /**
     * @param string $password
     * @param string $hashedPassword
     * @dataProvider falsePasswordProvider
     * @expectedException \Exception
     */
    public function testFailedVerify($password, $hashedPassword)
    {
        $this->expectException('Exception');
        $passwordHash = $this->password->verify($password, $hashedPassword);
        $this->assertEquals($passwordHash, false);
    }
    

    /**
     * @return array
     */
    public function hashPasswordProvider()
    {
        return array(
            ['password'],
            ['shortpass'],
            ['rlysht'],
            ['8978798796785897890908'],
            ['alongpasswordwithnumbers78890231908132'],
            ['areallylongpasswordwithnumbersandspecialchars-890988908!98099___%%£213321'],
            ['somethingsolong&*()&*(&*(&**@((@)_)@__@__@)))))@((((@**@*&&&@**@()(@))@_t132123hatitsalmostcertainlyno____tgoingtobeusedinproduct123312123ionunlesssomeone____decidedthattheyarehappywrittingthiseverytimetheywishtologintothesite_withsomenumbersandspecialcharacters']
        );
    }

    /**
     * @return array
     */
    public function truePasswordProvider()
    {
        return array(
            ['password',\password_hash('password', PASSWORD_DEFAULT)],
            ['foobar',\password_hash('foobar', PASSWORD_DEFAULT)],
            ['234234234cookie',\password_hash('234234234cookie', PASSWORD_DEFAULT)],
            ['fobatraz56278',\password_hash('fobatraz56278', PASSWORD_DEFAULT)],
            ['foo__fgg__bat__789',\password_hash('foo__fgg__bat__789', PASSWORD_DEFAULT)],
            ['passwdsasdads2123ord',\password_hash('passwdsasdads2123ord', PASSWORD_DEFAULT)]
        );
    }

    /**
     * @return array
     */
    public function falsePasswordProvider()
    {
        return array(
            ['password','$2y$10dBPLe66sadkE.RW'],
            ['password','cGFzc3dvcmQ='],
            ['password','5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8'],
            ['password','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'],
            ['password','74dfc2b2s3a5caee29ccad3557247eda238831b3e9bd931b01d77fe994e4f12b9d4cfa92a124461d2065197d8cf7f33fc88566da2db2a4d6eae'],
            ['password','db4d9992897eda89b50f1d3208db607902da7e79c6f3bc6e6933cc5919068564'],
            ['password','35c246d5'],
            ['password','ycbTFtbcTZUqeJ/UuIWO1w=='],
            ['password','c9c6d316d6dc4d952a789fd4b8858ed7'],
            ['password','c9:c6:d3:16:d6:dc:4d:95:2a:78:9f:d4:b8:85:8e:d7'],
        );
    }
}
