<?php

namespace Tests\Authenticate;

use Ds\Authenticate\HashHmac;

/**
 * Class HashHmacTest
 *
 * @package Tests\Authenticate
 */
class HashHmacTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \ReflectionClass
     */
    public $reflector;

    /**
     * @var string
     */
    public $algo;

    /**
     * @var array
     */
    public $data;

    /**
     * @var HashHmac
     */
    public $hmac;

    /**
     *
     */
    protected function setUp() : void
    {
        $this->algo = 'sha512';
        $this->privateKey = 'privateKey';
        $this->reflector = new \ReflectionClass(HashHmac::class);
        $this->data = ['foo' => 'bar'];
        $this->hmac = new HashHmac($this->privateKey,$this->algo);
    }

    /**
     *
     */
    public function testHash()
    {
        $actual = $this->hmac->create($this->data);
        $token = base64_decode($actual);
        $tokenParts = explode('=',$token);
        $tokenHash = $tokenParts[1];
        $expected = hash_hmac($this->algo, json_encode($this->data) , $this->privateKey);
        $this->assertEquals($tokenHash, $expected);
    }

    /**
     *
     */
    public function testCompareHash()
    {
        $encodedHash = $this->hmac->create($this->data);
        $this->assertEquals(true, $this->hmac->verify($encodedHash,$this->data));
    }

    /**
     *
     */
    public function testCompareHashInvalidAlgo()
    {
        $this->expectException('Exception');
        $hash = 'shafoo2=647a6cbf9b30cb8cc3fd5147a8fd30a19775fe3290e3dd0e729ae2f1e8a485f833469df616374fa923fcc3533545a11e6deba25a63bcd1ec81c316c090890ddc=10f3e8ed9c5c8b33cadda267882fa6449f5af465bc1dc420=1457010889';
        $encoded = base64_encode($hash);
        $this->hmac->verify($encoded, $this->data);
    }

    /**
     *
     */
    public function testCompareHashNonMatch()
    {

        $this->expectException('Exception');
        $encodedHash = $this->hmac->create($this->data);
        $differentPayload = ['foos' => 'bat'];
        $this->hmac->verify($encodedHash, $differentPayload);
    }

    /**
     *
     */
    public function testVerify()
    {
        $token = $this->hmac->create($this->data);
        $expected = true;
        $actual = $this->hmac->verify($token, $this->data);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testVerifyHashWrongKey()
    {
        $this->expectException('Exception');
        $expected = false;

        $newHmac = $this->hmac->withPrivateKey('some-random-key');
        $token = $newHmac->create($this->data);

        $actual = $this->hmac->verify($token, $this->data);
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testVerifyHashPayloadMismatch()
    {
        $this->expectException('Exception');
        $data = md5('someOtherStuff');
        $actualToken = $this->hmac->create(['data' => 'payload']);
        $this->hmac->verify($actualToken, ['data' => $data]);
    }

    /**
     *
     */
    public function testGetPrivateKey()
    {
        $expected = 'privateKey';
        $actual = $this->hmac->getPrivateKey();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testWithPrivateKey()
    {
        $expected = 'newKey';
        $newKey = $this->hmac->withPrivateKey($expected);
        $this->assertEquals($expected, $newKey->getPrivateKey());
    }

    /**
     *
     */
    public function testGetAlgorithm()
    {
        $expected = 'sha512';
        $actual = $this->hmac->getAlgorithm();
        $this->assertEquals($expected, $actual);
    }

    /**
     *
     */
    public function testVerifyAlgorithm()
    {
        $method = $this->reflector->getMethod('verifyAlgorithm');
        $method->setAccessible(true);
        $actual = $method->invokeArgs($this->hmac, ['sha256']);
        $this->assertEquals($actual, 'sha256');
    }

    /**
     *
     */
    public function testVerifyAlgorithmException()
    {
        $this->expectException('Exception');

        $method = $this->reflector->getMethod('verifyAlgorithm');
        $method->setAccessible(true);
        $method->invokeArgs($this->hmac, ['sha111']);
    }

}
