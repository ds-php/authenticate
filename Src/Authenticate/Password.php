<?php

namespace Ds\Authenticate;

/**
 * Class Password
 * @package Ds\Authenticate
 */
class Password implements PasswordInterface
{

    /**
     * Verify Password hash.
     *
     * @param  string $password
     * @param  string $hash
     * @return bool
     * @throws \Exception
     */
    public function verify($password, $hash)
    {
        if (\password_verify($password, $hash)) {
            return true;
        }
        throw new \Exception('Verify failed. Non matching hash');
    }

    /**
     * Check if provided password needs rehashing with new Algorithm.
     *
     * @param  string $password
     * @param  string $hash
     * @param  int    $algo
     * @param  int    $cost
     * @return bool|string
     */
    public function needsRehash($password, $hash, $algo = PASSWORD_DEFAULT, $cost = 12)
    {
        if (\password_needs_rehash($hash, $algo, ['cost' => $cost])) {
            return $this->hash($password, $algo, $cost);
        }
        return false;
    }

    /**
     * Create hash from string.
     *
     * @param  string $password
     * @param  int    $algo
     * @param  int    $cost
     * @return string
     */
    public function hash($password, $algo = PASSWORD_DEFAULT, $cost = 12)
    {
        return \password_hash($password, $algo, ['cost' => $cost]);
    }
}
