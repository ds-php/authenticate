<?php

namespace Ds\Authenticate;

/**
 * Interface PasswordInterface
 * @package Ds\Authenticate
 */
Interface PasswordInterface
{
    /**
     * Verify Password hash.
     *
     * @param  string $password
     * @param  string $hash
     * @return bool
     * @throws \Exception
     */
    public function verify($password, $hash);

    /**
     * Create hash from string.
     *
     * @param  string $password
     * @param  int    $algo
     * @param  int    $cost
     * @return string
     */
    public function hash($password, $algo = PASSWORD_DEFAULT, $cost = 12);


    /**
     * Check if provided password needs rehashing with new Algorithm.
     *
     * @param  string $password
     * @param  string $hash
     * @param  int    $algo
     * @param  int    $cost
     * @return bool|string
     */
    public function needsRehash($password, $hash, $algo = PASSWORD_DEFAULT, $cost = 12);

}
