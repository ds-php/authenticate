<?php

namespace Ds\Authenticate;

/**
 * Class Token
 * @package Ds\Authenticate
 */
class Token implements TokenInterface
{

    /**
     * Create a random token.
     *
     * @param  int $length
     * @return string
     */
    public function create($length = 40)
    {
        $token = null;
        while ($token === null) {
            $token = $this->generateRandomBytes($length);
        }
        return $token;
    }

    /**
     * Generate Random byes.
     *
     * @param  $length
     * @return string
     */
    public function generateRandomBytes($length)
    {
        return \openssl_random_pseudo_bytes($length);
    }

    /**
     * Create a token from a string + key.
     *
     * @param  string $string
     * @param  string $key
     * @param  string $algo
     * @return string
     */
    public function createFromString($string, $key, $algo = 'sha256')
    {
        return \bin2hex(\hash_hmac($algo, $string, $key));
    }
}
