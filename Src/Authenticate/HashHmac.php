<?php

namespace Ds\Authenticate;

/**
 * Hmac hashing functions.
 *
 * @package Ds\Authenticate
 * @author  Dan Smith <dan--smith@hotmail.co.uk>
 */
class HashHmac
{

    /**
     * Hashing Algorithm.
     *
     * @var string
     */
    private $algorithm;

    /**
     * Private Key.
     *
     * @var string
     */
    private $privateKey;

    /**
     * Payload Data.
     *
     * @var array
     */
    private $payload = [];

    /**
     * Date Time Zone.
     *
     * @var \DateTimeZone
     */
    private $timezone;

    /**
     * @param string $privateKey
     * @param string $algo
     * @param string $timezone
     */
    public function __construct($privateKey, $algo = 'sha512', $timezone = 'UTC')
    {
        $this->privateKey = $privateKey;
        $this->algorithm = $algo;
        $this->timezone = new \DateTimeZone($timezone);
    }


    /**
     * Create Hash Hmac from payload.
     *
     * @param array $payload
     * @return string
     */
    public function create(array $payload = []){
        $hash = $this->generate($this->algorithm, $payload, $this->privateKey);
        return base64_encode($this->algorithm.'='.$hash);
    }

    /**
     * Internal Function to creates hash from payload.
     *
     * @param  array $payload
     * @return mixed
     */
    private function generate($algorithm, array $payload = [], $privateKey)
    {
        $this->payload = json_encode($payload);

        return hash_hmac(
            $algorithm,
            $this->payload,
            $privateKey
        );
    }

    /**
     * Compare received hash against payload.
     *
     * @param  $encodedHash
     * @param  array       $payload
     * @return bool
     * @throws \Exception
     */
    public function verify($encodedHash, array $payload = [])
    {
        $decodedHash = base64_decode($encodedHash);

        list($algo, $hash) = array_pad( explode('=', $decodedHash), 4, $this->algorithm);

        $payloadHash = $this->generate($this->algorithm, $payload, $this->privateKey);

        try {
            $this->verifyAlgorithm($algo);
            $this->verifyHash($hash, $payloadHash);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return true;

    }

    /**
     * With Private Key.
     *
     * @param string $key
     * @return $this
     */
    public function withPrivateKey(string $key)
    {
        $new = clone $this;
        $new->privateKey = $key;
        return $new;
    }

    /**
     * With Payload Data
     *
     * @param array $payload
     * @return $this
     */
    public function withPayload(array $payload)
    {
        $new = clone $this;
        $new->payload = $payload;
        return $new;
    }

    /**
     * Return current Algorithm
     *
     * @return string
     */
    public function getAlgorithm()
    {
        return $this->algorithm;
    }


    /**
     * Return active payload.
     *
     * @return array
     */
    public function getPayload(){
        return $this->payload;
    }

    /**
     * Returns Private Key
     *
     * @return string
     */
    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    /**
     * @param string $hash
     * @param string $payloadHash
     * @return bool
     * @throws \Exception
     */
    private function verifyHash($hash = '', $payloadHash = '')
    {
        if (!is_string($hash) || !is_string($payloadHash)) {
            throw new \Exception('hashes are not of the same type');
        }

        if (\md5($hash) === \md5($payloadHash)
            && strlen($hash) === strlen($payloadHash)
            && hash_equals($hash, $payloadHash)
        ) {
            return true;
        }

        throw new \Exception('hashes do not match');
    }

    /**
     * @param $algo
     * @return string
     * @throws \Exception
     */
    private function verifyAlgorithm($algo)
    {

        foreach (hash_algos() as $validAlgo) {
            if ($algo === $validAlgo) {
                $this->algorithm = $validAlgo;
                return $this->algorithm;
            }
        }
        throw new \Exception('Algorithm is not valid');
    }
}
