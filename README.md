# Authenticate
Authentication Classes


[![SensioLabsInsight](https://insight.sensiolabs.com/projects/a7683470-5532-4b30-8707-f6e8d9e37556/big.png)](https://insight.sensiolabs.com/projects/a7683470-5532-4b30-8707-f6e8d9e37556)

Travis:
[![Build Status](https://travis-ci.org/djsmithme/Authenticate.svg?branch=master)](https://travis-ci.org/djsmithme/Authenticate)

Code Climate:
[![Code Climate](https://codeclimate.com/github/djsmithme/Authenticate/badges/gpa.svg)](https://codeclimate.com/github/djsmithme/Authenticate)
[![Test Coverage](https://codeclimate.com/github/djsmithme/Authenticate/badges/coverage.svg)](https://codeclimate.com/github/djsmithme/Authenticate/coverage)

Scrutinizer:
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/djsmithme/Authenticate/badges/quality-score.png?branch=master)](https://scrutinizer-ci.com/g/djsmithme/Authenticate/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/g/djsmithme/Authenticate/badges/coverage.png?branch=master)](https://scrutinizer-ci.com/g/djsmithme/Authenticate/?branch=master)
[![Build Status](https://scrutinizer-ci.com/g/djsmithme/Authenticate/badges/build.png?branch=master)](https://scrutinizer-ci.com/g/djsmithme/Authenticate/build-status/master)

##Token
```
$token = new \Ds\Authenticate\Token();
$randomToken = $token->create();
var_dump($randomToken);

$stringToken = $token->createFromString('this is a test','privateKey');
var_dump($stringToken);
```
##Password

```
$password = new \Ds\Authenticate\Password();
```

```
$examplePassword = 'password';
```

```
$hashedExamplePassword = $password->hash($examplePassword,PASSWORD_DEFAULT);
var_dump($hashedExamplePassword);
```

```
try{
  $verified = $password->verify($examplePassword, $hashedExamplePassword);
  $updatedPassword = $password->needsRehash($hashedExamplePassword);
  if ($hashedExamplePassword !== $updatedPassword){
    //save updated password
  }
catch(\Exception $e){
  //not authenticated...
}
```

